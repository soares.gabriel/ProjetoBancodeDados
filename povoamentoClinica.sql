SET search_path TO clinica_katharine_gabriel_paulo;

/* Povoamento de Ceps */

INSERT INTO cep(
    idCEP, estado, cidade, bairro, logradouro, cep)
    VALUES (1,'Sergipe','Aracaju','Robalo','Rua Antônio Barbosa','49004-709'),
    (2,'Sergipe','Aracaju','Treze de Julho','Rua Álvaro Silva','49020-260'),
    (3,'Sergipe','Aracaju','Treze de Julho','Rua Cristóvão de Barros','49020-180'),
    (4,'Sergipe','Aracaju','Treze de Julho','Rua Capitão Benedito Teófilo Otoni','49020-050'),
    (5,'Sergipe','Aracaju','Treze de Julho','Rua Doutor Celso Oliva','49020-090'),
    (6,'Sergipe','Aracaju','Centro','Largo Antônio Franco','49010-690'),
    (7,'Sergipe','Aracaju','Centro','Largo dos Previdenciários','49010-253'),
    (8,'Sergipe','Aracaju','Centro','Largo Esperanto','49010-193'),
    (9,'Sergipe','Aracaju','Centro','Rua Capela','49010-370'),
    (10,'Sergipe','Aracaju','Centro','Rua Geru','49010-460');

INSERT INTO funcionario (
    idFuncionario, pnome, snome, data_nascimento, data_cadastro, telefone_residencial, telefone_celular, cpf, email, cargo, salario, numero, pais, idcep) 

        VALUES (1,'Hayes','Oneil','1971-11-14','2016-11-21 10:27:23','(79) 9818-0532','(79) 9208-7380','1678050918699','nibh.Phasellus@Sednuncest.net','Veterinario','9077',216,'Brasil',3),
        (2,'Adam','Golden','1971-04-09','1985-11-01 14:26:07','(79) 9402-7474','(79) 9965-8977','1659073016599','Nam@acfacilisis.ca','Veterinario','7734',321,'Brasil',9),
        (3,'Kennedy','Washington','1970-04-27','2016-06-06 16:47:23','(79) 9324-4405','(79) 9745-0719','1695101909999','risus.varius.orci@Integervulputaterisus.co.uk','Veterinario','7463',171,'Brasil',9),
        (4,'Carlos','Knowles','1985-05-09','2018-01-07 20:51:28','(79) 9406-8654','(79) 9525-6918','1672061378999','pharetra.Nam@Nunc.com','Secretaria','7187',333,'Brasil',10),
        (5,'Tereza','Hines','1975-10-23','2017-10-15 08:38:36','(79) 9500-2898','(79) 9988-4047','1632080318199','In.ornare.sagittis@elementum.org','Secretaria','5497',159,'Brasil',4),
        (6,'Carmem','Rice','1990-04-03','2017-09-11 15:58:56','(79) 9360-7469','(79) 9511-0619','1688082808999','enim.Etiam.gravida@ultriciesdignissimlacus.net','Secretaria','7114',275,'Brasil',9),
        (7,'Todd','Harrell','1983-12-26','2018-02-02 18:26:52','(79) 9842-5052','(79) 9892-5211','1616062605899','malesuada@malesuada.org','Recepcionista','6101',350,'Brasil',8),
        (8,'Edan','Wiggins','1982-03-09','2016-08-31 14:14:58','(79) 9303-8445','(79) 9420-2829','1618110477799','id.magna@Aeneanegestas.co.uk','Recepcionista','6543',394,'Brasil',8),
        (9,'Adrew','Hooper','1981-09-17','2016-07-09 09:36:17','(79) 9760-7149','(79) 9959-3582','1632073082599','diam.vel@In.edu','Veterinario','6385',47,'Brasil',6),
        (10,'Carla','Pate','1983-11-26','2016-08-12 01:23:36','(79) 9250-6314','(79) 9172-6381','1624061341899','Fusce@Morbinon.edu','Veterinario','5967',295,'Brasil',5);


INSERT INTO secretaria(
    idfuncionario)
    VALUES (4),(5),(6);

INSERT INTO veterinario(
    idfuncionario, crmv, idsecretaria)
    VALUES (1, 'CRMV-SE 237495', 5),(2, 'CRMV-SE 468415', 4), (3, 'CRMV-SE 567329', 6), (9, 'CRMV-SE 036478', 4), (10, 'CRMV-SE 879651', 5);

INSERT INTO recepcionista(
    idfuncionario)
    VALUES (7),(8);


INSERT INTO especie(
    idespecie, especie, raca)
    VALUES (1, 'canino', 'SRD'),
            (2, 'canino', 'Akita'),
            (3, 'canino', 'Xau Xau'),
            (4, 'canino', 'Yorkshire'),
            (5, 'canino', 'Poodle'),
            (6, 'felino', 'Siamês'),
            (7, 'felino', 'Maine Coon'),
            (8, 'felino', 'Norueguês da Floresta'),
            (9, 'felino', 'Persa'),
            (10, 'felino', 'SRD');

INSERT INTO cliente(
    idcliente, pnome, snome, data_nascimento, telefone_residencial, telefone_celular, cpf, email, profissao, numero, pais, idcep)
    VALUES (1,'Coby','Blackburn','1990-01-27','(79) 9676-9505','(79) 9604-7963','1625022880299','eleifend.non@dapibus.co.uk','Professor',242,'Brasil',4),
    (2,'Ronan','Sawyer','1980-07-22','(79) 9543-8416','(79) 9551-4643','1659022626499','massa.Quisque.porttitor@convalliserateget.com','Bibliotecário',295,'Brasil',7),
    (3,'Katherine','Justice','1981-04-21','(79) 9886-0977','(79) 6948-4599','1608093040099','euismod@ligula.com','Médico',128,'Brasil',10),
    (4,'Henry','Olsen','1983-04-07','(79) 9153-2809','(79) 9772-3164','1675091245399','eu@accumsannequeet.net','Militar',271,'Brasil',10),
    (5,'Justin','Webb','1985-06-23','(79) 9178-9516','(79) 9763-0626','1666021147699','eget@egetmagnaSuspendisse.org','Engenheiro Civil',50,'Brasil',10),
    (6,'Marcela','Rocha','1971-04-26','(79) 9858-7997','(79) 9100-9043','1610080145899','arcu.imperdiet.ullamcorper@NuncmaurisMorbi.net','Advogado',377,'Brasil',8),
    (7,'Castor','Cash','1982-06-18','(79) 9504-3091','(79) 9237-5745','1643010735699','per.inceptos@iaculisaliquetdiam.edu','Autônomo',390,'Brasil',8),
    (8,'Taylor','Espinoza','1981-12-12','(79) 9657-5816','(79) 9871-6443','1624020491399','cursus@eu.com','Lacus Company',131,'Professor',2),
    (9,'Burton','Bowers','1981-02-13','(79) 9974-7489','(79) 9569-2039','1631041383499','nulla.magna@fermentumvelmauris.co.uk','Médico',118,'Brasil',1),
    (10,'Samantha','Hensley','1973-03-09','(79) 9561-3989','(79) 9392-7545','1690040716499','volutpat.Nulla.facilisis@lacusvestibulumlorem.org','Arquiteto',283,'Brasil',9);

INSERT INTO animal (
    idanimal, nome, sexo, data_nascimento, idcliente, idespecie) 
    VALUES  (1, 'Bobbafett', 'M', '2007-12-03', 1, 1),
        (2, 'Bodie', 'M', '2016-12-16', 2, 2),
        (3, 'Bono', 'M', '2006-10-07', 3, 3),
        (7, 'Booboo', 'M', '2005-10-08', 7, 7),
        (11, 'Bootsie', 'M', '2005-01-01', 4, 5),
        (15, 'Acorn', 'M', '2006-02-04', 2, 9),
        (17, 'Alvin', 'M', '2005-08-05', 2, 2),
        (4, 'Chevy', 'F', '2015-03-15', 4, 4),
        (5, 'China', 'F', '2012-06-12', 5, 5),
        (6, 'Choochoo', 'F', '2014-12-14', 6, 6),
        (8, 'Cisca', 'F', '2009-10-09', 8, 8),
        (9, 'Claire', 'F', '2007-08-07', 9, 9),
        (10, 'Cleopatra', 'F', '2010-12-10', 10, 10),
        (12, 'Sushi', 'F', '2008-10-08', 5, 6),
        (13, 'Sunny', 'F', '2003-07-03', 6, 7),
        (14, 'Summer', 'F', '2006-10-06', 7, 8),
        (16, 'Sugar', 'F', '2012-07-13', 3, 1),
        (18, 'Strsky', 'F', '2010-02-10', 5, 3),
        (19, 'Stitch', 'F', '2013-02-13', 1, 4),
        (20, 'Indira', 'F', '2004-01-04', 1, 5);

INSERT INTO fornecedor (
    idfornecedor, nome_real, nome_fantasia, data_cadastro, email, responsavel, telefone, cnpj, numero, pais, idcep)
    VALUES (1, 'Non Leo LLC', 'Companhia Neon', '2016-01-21 14:27:23', 'nonleo@email.ca', 'Carlos de Melo', '(79) 9541-3329', 42724760000191, 235, 'Brasil', 2),
    (2, 'Commodo Ipsum Inc.', 'Commodo Fornecedores', '2016-02-21 16:24:33', 'commodoipsum@email.ca', 'Stephanie Souza', '(79) 9221-5559',  62014196000120, 1054, 'Brasil', 3),
    (3, 'Proin LLC', 'Proin Distribuidora', '2017-01-21 10:31:53', 'proin@email.ca', 'Maria do Carmo', '(79) 9333-7329', 06756462000110, 2456, 'Brasil', 7),
    (4, 'Porttitor Incorporated', 'Porttitor Distribuidora', '2017-02-11 12:45:21', 'porttitor@email.ca', 'Antonio Rocha', '(79) 9576-3969',  67089476000111, 420, 'Brasil', 1),
    (5, 'Ipsum Foundation', 'Ipsum Distribuidora', '2017-03-16 10:27:23', 'ipsum@email.ca', 'Francisco Carvalho', '(79) 9863-8532', 52345443000118, 653, 'Brasil', 5);


INSERT INTO produto (
    idproduto, nome, descricao, qtd_itens, val_compra, val_revenda) 
    VALUES  (1,'Sure Grow 100','Suplemento para filhotes caninos',92,62.83,75.40),
        (2,'Terramycin Ophthalmic Ointment','Antibiótico pomada para infecções oculares',64,144.51,173.42),
        (3,'ADEQID® 12mm  Microchip','Microchips para pets',16,289.03,346.83),
        (4,'ADEQID® 8.5mm  Microchip','Microchips para pets',16,311.02,373.22),
        (5,'Advantage II for Cats','Anti-pulgas e piolhos para felinos',85,179.07,214.88);


INSERT INTO fornecedor_has_produto(
    idfornecedor, idproduto)
    VALUES (1, 2),
            (2, 3),
            (3, 5),
            (4, 1),
            (5, 4),
            (5, 2),
            (3, 1);

INSERT INTO perfil(
    idperfil, login, senha, idfuncionario)
    VALUES (1, 'hayeso', '12345678', 1),
    (2, 'adamg', '12345678', 2),
    (3, 'kennedyw', '12345678', 3),
    (4, 'carlosk', '12345678', 4),
    (5, 'terezah', '12345678', 5),
    (6, 'carmemr', '12345678', 6),
    (7, 'toddh', '12345678', 7),
    (8, 'edanw', '12345678', 8),
    (9, 'adrewh', '12345678', 9),
    (10, 'carlap', '12345678', 10);


INSERT INTO consulta(
    idconsulta, data_cadastro, data_marcacao, resumo, valor, status, lista_prod, idcliente, idanimal, idsecretaria, idveterinario)
    VALUES 
    (1,'2017-01-04 16:22:21','2017-11-28 09:24:17','In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet.',337.09,'realizada','3,1,5',5,18,5,1),
    (2,'2017-01-18 17:56:43','2017-10-17 08:14:53','Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel, egestas et, gue.',231.22,'realizada','3,4,5',1,19,6,2),
    (3,'2017-01-23 12:37:28','2017-01-24 15:34:23','Donec lacus nunc, viverra nec, blandit vel, egestas et, gue. Vestibulum tincidunt malesuada tellus.',975.57,'realizada','4,1,2',1,20,5,3),
    (4, '2017-02-10 10:27:23', '2017-02-13 10:47:25', '', '90.00', 'agendada', '', 1, 1, 5, 1),
    (5, '2017-03-13 10:47:25', '2017-03-13 10:47:25', 'Urna justo faucibus lectus, a sollicitudin orci sem eget massa.', '110.00', 'realizada', '3,4', 1, 1, 6, 3),
    (6, '2017-04-11 09:34:13', '2017-04-10 15:24:23', 'Malesuada fames ac turpis egestas. Aliquam fringilla cursus purus.', '90.00', 'realizada', '1,3,4', 4, 4, 5, 10);


INSERT INTO historico(
    idhistorico, idconsulta, data_cadastro)
    VALUES (1, 1, '2017-01-04 16:22:21'),
    (2, 2, '2017-01-18 17:56:43'),
    (3, 3, '2017-02-23 12:37:28'),
    (4, 4, '2017-02-10 10:27:23'),
    (5, 5, '2017-03-13 10:47:25'),
    (6, 6, '2017-04-11 09:34:13');


INSERT INTO acesso(
    idacesso, idhistorico, idperfil, idveterinario, data_cadastro)
    VALUES (1, 4, 1, 1, '2017-04-04 10:46:34'),
    (2, 5, 3, 3, '2017-04-04 10:46:34'),
    (3, 1, 1, 1, '2017-04-05 06:51:43'),
    (4, 3, 3, 3, '2017-04-02 08:12:16'),
    (5, 6, 10, 10, '2017-04-24 10:46:34');


INSERT INTO itemproduto (iditemproduto, tipo, idproduto, idconsulta) 
    VALUES  (1,'eletrônico',3,5),
        (2,'eletrônico',4,5),
        (3,'consumível',1,6),
        (4,'eletrônico',3,6),
        (5,'eletrônico',4,6),
        (6,'eletrônico',3,1),
        (7,'consumível',1,1),
        (8,'consumível',5,1),
        (9,'eletrônico',3,2),
        (10,'eletrônico',4,2),
        (11,'consumível',5,2),
        (12,'eletrônico',4,3),
        (13,'consumível',1,3),
        (14,'consumível',2,3);                        
